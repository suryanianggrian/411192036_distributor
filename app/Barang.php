<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class barang extends Model
{


    protected $table = 'barang';
    protected $primarykey = 'id';

    protected $fillable = [
        'kode_barang','nama_barang','deskripsi', 'stok_barang', 'harga_barang'
    ];
}
