@extends('template')

@section('content')
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>Create New Pembelian</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-secondary" href="{{ route('pembelian.index') }}"> Back</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Input gagal.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('pembelian.store') }}" method="POST">
    @csrf

     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>No Pembelian :</strong>
                <input type="text" maxlength="15" name="no_pembelian" class="form-control" placeholder="PL0001">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tanggal:</strong>
                <input type="date" name="tanggal" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>ID Supplier :</strong>
                <input type="number" maxlength="15" name="id_supplier" class="form-control" placeholder="000001">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group{{ $errors->has('id_barang') ? ' alert alert-danger' : '' }}">
                                    <strong>ID Barang:</strong>
                                    <select name="id_barang" class="form-control @error('id_barang') is-invalid @enderror">
                                      <option value="">-- PILIH --</option>
                                      @foreach ($barang as $item)
                                        <option value="{{ $item->id }}" {{ old('id_barang') == $item->id ? 'selected' : null }}>
                                          {{ $item->nama_barang }}
                                        </option>
                                      @endforeach
                                    </select>
                                    @if($errors->has('id_barang'))
                                    <span class="help-block">
                                      <strong>{{ $errors->first('id_barang') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
             <div class="form-group">
                <strong>Jumlah Barang:</strong>
                    <input type="number" maxlength="99" name="jumlah_barang" class="form-control" placeholder="MAX INPUT 99"  >
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Harga Barang:</strong>
                <input type="number" maxlength="99" name="harga_barang" class="form-control" placeholder="MAX INPUT 99"  >
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@endsection