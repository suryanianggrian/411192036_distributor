<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {
        return view('template');
    });
    
    Route::resource('barang', BarangController::class);
    Route::resource('penjualan', PenjualanController::class);
    Route::resource('pelanggan', PelangganController::class);
    Route::resource('pembelian', PembelianController::class);
    Route::resource('supplier', SupplierController::class);
});